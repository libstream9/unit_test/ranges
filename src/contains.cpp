#include <stream9/ranges/contains.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

#include <vector>

namespace testing {

BOOST_AUTO_TEST_SUITE(contains_)

    BOOST_AUTO_TEST_CASE(success_)
    {
        std::vector<int> v { 1, 2, 3, 4, 5 };

        BOOST_TEST(rng::contains(v, 3));
    }

    BOOST_AUTO_TEST_CASE(fail_)
    {
        std::vector<int> v { 1, 2, 3, 4, 5 };

        BOOST_TEST(!rng::contains(v, 0));
    }

BOOST_AUTO_TEST_SUITE_END() // contains_

} // namespace testing
