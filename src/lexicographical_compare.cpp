#include <stream9/ranges/lexicographical_compare.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(lexicographical_compare_)

    BOOST_AUTO_TEST_CASE(iterator_1_)
    {
        auto r1 = { 1, 2, 3 };
        auto r2 = { 1, 2, };

        auto c = rng::lexicographical_compare(
            r1.begin(), r1.end(),
            r2.begin(), r2.end()
        );

        BOOST_CHECK(c == 1);
    }

    BOOST_AUTO_TEST_CASE(iterator_2_)
    {
        auto r1 = { 1, 2, 3 };
        auto r2 = { 1, 2, 3 };

        auto c = rng::lexicographical_compare(
            r1.begin(), r1.end(),
            r2.begin(), r2.end()
        );

        BOOST_CHECK(c == 0);
    }

    BOOST_AUTO_TEST_CASE(iterator_3_)
    {
        auto r1 = { 1, 2 };
        auto r2 = { 1, 2, 3 };

        auto c = rng::lexicographical_compare(
            r1.begin(), r1.end(),
            r2.begin(), r2.end()
        );

        BOOST_CHECK(c == -1);
    }

    BOOST_AUTO_TEST_CASE(range_1_)
    {
        auto r1 = { 1, 2, };
        auto r2 = { 1, 2, 3 };

        auto c = rng::lexicographical_compare(
            r1, r2
        );

        BOOST_CHECK(c == -1);
    }

    BOOST_AUTO_TEST_CASE(range_2_)
    {
        auto r1 = { 1, 2, 3 };
        auto r2 = { 1, 2, 3 };

        auto c = rng::lexicographical_compare(
            r1, r2
        );

        BOOST_CHECK(c == 0);
    }

    BOOST_AUTO_TEST_CASE(range_3_)
    {
        auto r1 = { 1, 2, 3 };
        auto r2 = { 1, 2 };

        auto c = rng::lexicographical_compare(
            r1, r2
        );

        BOOST_CHECK(c == 1);
    }

    auto abs = [](auto x) {
        return x < 0 ? -x : x;
    };

    auto abs_less = [](auto x, auto y) {
        return abs(x) < abs(y);
    };

    BOOST_AUTO_TEST_CASE(range_4_)
    {
        auto r1 = { 1, 2, };
        auto r2 = { -1, -2, -3 };

        auto c = rng::lexicographical_compare(
            r1, r2, abs_less
        );

        BOOST_CHECK(c == -1);
    }

    BOOST_AUTO_TEST_CASE(range_5_)
    {
        auto r1 = { 1, 2, 3 };
        auto r2 = { -1, -2, -3 };

        auto c = rng::lexicographical_compare(
            r1, r2, abs_less
        );

        BOOST_CHECK(c == 0);
    }

    BOOST_AUTO_TEST_CASE(range_6_)
    {
        auto r1 = { 1, 2, 3 };
        auto r2 = { -1, -2 };

        auto c = rng::lexicographical_compare(
            r1, r2, abs_less
        );

        BOOST_CHECK(c == 1);
    }

BOOST_AUTO_TEST_SUITE_END() // lexicographical_compare_

} // namespace testing
