#include <stream9/ranges/subrange.hpp>

#include "namespace.hpp"

#include <stream9/errors.hpp>
#include <stream9/json/value_from.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(subrange_)

    BOOST_AUTO_TEST_CASE(case_1_)
    {
        int arr[] = { 1, 2, 3, 4, 5 };

        auto x = rng::subrange(arr, 1, 3);

        json::array expected { 2, 3, 4 };
        BOOST_TEST(json::value_from(x) == expected);
    }

    BOOST_AUTO_TEST_CASE(case_2_)
    {
        int arr[] = { 1, 2, 3, 4, 5 };

        auto x = rng::subrange(arr, 0, 0);

        json::array expected { };
        BOOST_TEST(json::value_from(x) == expected);
    }

    BOOST_AUTO_TEST_CASE(case_3_)
    {
        int arr[] = { 1, 2, 3, 4, 5 };

        auto x = rng::subrange(arr, 5, 0);

        json::array expected { };
        BOOST_TEST(json::value_from(x) == expected);
    }

    BOOST_AUTO_TEST_CASE(case_4_)
    {
        int arr[] = { 1, 2, 3, 4, 5 };

        auto x = rng::subrange(arr, -1, 1);

        json::array expected { 5 };
        BOOST_TEST(json::value_from(x) == expected);
    }

    BOOST_AUTO_TEST_CASE(case_5_)
    {
        int arr[] = { 1, 2, 3, 4, 5 };

        auto x = rng::subrange(arr, -5, 10);

        json::array expected { 1, 2, 3, 4, 5 };
        BOOST_TEST(json::value_from(x) == expected);
    }

    BOOST_AUTO_TEST_CASE(case_6_)
    {
        int arr[] = { 1, 2, 3, 4, 5 };

        auto x = rng::subrange(arr, -6, 3);

        json::array expected { 1, 2, 3 };
        BOOST_TEST(json::value_from(x) == expected);
    }

    BOOST_AUTO_TEST_CASE(case_7_)
    {
        int arr[] = { 1, 2, 3, 4, 5 };

        auto x = rng::subrange(arr, 0);

        json::array expected { 1, 2, 3, 4, 5 };
        BOOST_TEST(json::value_from(x) == expected);
    }

    BOOST_AUTO_TEST_CASE(case_8_)
    {
        int arr[] = { 1, 2, 3, 4, 5 };

        auto x = rng::subrange(arr, 2);

        json::array expected { 3, 4, 5 };
        BOOST_TEST(json::value_from(x) == expected);
    }

    BOOST_AUTO_TEST_CASE(case_9_)
    {
        int arr[] = { 1, 2, 3, 4, 5 };

        auto x = rng::subrange(arr, 5);

        json::array expected { };
        BOOST_TEST(json::value_from(x) == expected);
    }

    BOOST_AUTO_TEST_CASE(case_10_)
    {
        int arr[] = { 1, 2, 3, 4, 5 };

        auto x = rng::subrange(arr, -1);

        json::array expected { 5 };
        BOOST_TEST(json::value_from(x) == expected);
    }

    BOOST_AUTO_TEST_CASE(case_11_)
    {
        int arr[] = { 1, 2, 3, 4, 5 };

        auto x = rng::subrange(arr, -6);

        json::array expected { 1, 2, 3, 4, 5 };
        BOOST_TEST(json::value_from(x) == expected);
    }

BOOST_AUTO_TEST_SUITE_END() // subrange_

} // namespace testing
