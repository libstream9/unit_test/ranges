#include <stream9/ranges/lexicographical_compare_three_way.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(lexicographical_compare_three_way_)

    BOOST_AUTO_TEST_CASE(iterator_1_)
    {
        auto r1 = { 1, 2, 3 };
        auto r2 = { 1, 2, };

        auto c = rng::lexicographical_compare_three_way(
            r1.begin(), r1.end(),
            r2.begin(), r2.end()
        );

        BOOST_CHECK(c == std::strong_ordering::greater);
    }

    BOOST_AUTO_TEST_CASE(iterator_2_)
    {
        auto r1 = { 1, 2, 3 };
        auto r2 = { 1, 2, 3 };

        auto c = rng::lexicographical_compare_three_way(
            r1.begin(), r1.end(),
            r2.begin(), r2.end()
        );

        BOOST_CHECK(c == std::strong_ordering::equivalent);
    }

    BOOST_AUTO_TEST_CASE(range_1_)
    {
        auto r1 = { 1, 2, };
        auto r2 = { 1, 2, 3 };

        auto c = rng::lexicographical_compare_three_way(
            r1, r2
        );

        BOOST_CHECK(c == std::strong_ordering::less);
    }

BOOST_AUTO_TEST_SUITE_END() // lexicographical_compare_three_way_

} // namespace testing
