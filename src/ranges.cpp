#include <stream9/ranges/at.hpp>
#include <stream9/ranges/back.hpp>
#include <stream9/ranges/boundary.hpp>
#include <stream9/ranges/front.hpp>
#include <stream9/ranges/index_at.hpp>
#include <stream9/ranges/iterator_at.hpp>
#include <stream9/ranges/last_index.hpp>

#include "namespace.hpp"

#include <stream9/ranges/find.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(ranges_)

    BOOST_AUTO_TEST_CASE(front_)
    {
        std::vector<int> v { 5, 4, 3, 2, 1 };

        BOOST_TEST(rng::front(v) == 5);
    }

    BOOST_AUTO_TEST_CASE(back_)
    {
        std::vector<int> v { 5, 4, 3, 2, 1 };

        BOOST_TEST(rng::back(v) == 1);
    }

    BOOST_AUTO_TEST_CASE(at_)
    {
        std::vector<int> v { 5, 4, 3, 2, 1 };

        BOOST_TEST(rng::at(v, 2) == 3);

        BOOST_CHECK_EXCEPTION(
            rng::at(v, 5),
            stream9::error,
            [&](auto&& e) {
                BOOST_TEST(e.why() == rng::errc::out_of_range);
                return true;
            });

        BOOST_CHECK_EXCEPTION(
            rng::at(v, -1),
            stream9::error,
            [&](auto&& e) {
                BOOST_TEST(e.why() == rng::errc::out_of_range);
                return true;
            });
    }

    BOOST_AUTO_TEST_CASE(index_at_)
    {
        std::vector<int> v { 5, 4, 3, 2, 1 };

        auto it = rng::find(v, 2);
        BOOST_TEST(rng::index_at(v, it) == 3);
    }

    BOOST_AUTO_TEST_CASE(last_index_)
    {
        std::vector<int> v { 5, 4, 3, 2, 1 };

        BOOST_TEST(rng::last_index(v) == 4);
    }

    BOOST_AUTO_TEST_CASE(iterator_at_)
    {
        std::vector<int> v { 5, 4, 3, 2, 1 };

        auto it = rng::find(v, 2);
        BOOST_CHECK(rng::iterator_at(v, 3) == it);
    }

BOOST_AUTO_TEST_SUITE_END() // ranges_

BOOST_AUTO_TEST_SUITE(boundary_)

    template<typename R>
    inline constexpr bool is_invocable =
        requires (R&& v) {
            rng::boundary(std::forward<R>(v));
        };

    BOOST_AUTO_TEST_CASE(lvalue_)
    {
        using R = std::vector<int>;
        R v;

        auto [i, e] = rng::boundary(v);

        static_assert(std::same_as<decltype(i), rng::iterator_t<R>>);
        static_assert(std::same_as<decltype(e), rng::sentinel_t<R>>);
    }

    BOOST_AUTO_TEST_CASE(const_lvalue_)
    {
        using R = std::vector<int>;
        R const v;

        auto [i, e] = rng::boundary(v);

        static_assert(std::same_as<decltype(i), rng::iterator_t<R const>>);
        static_assert(std::same_as<decltype(e), rng::sentinel_t<R const>>);
    }

    BOOST_AUTO_TEST_CASE(rvalue_)
    {
        static_assert(!is_invocable<std::vector<int>&&>);
    }

BOOST_AUTO_TEST_SUITE_END() // boundary_

} // namespace testing
