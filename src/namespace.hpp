#ifndef STREAM9_RANGES_TEST_SRC_NAMESPACE_HPP
#define STREAM9_RANGES_TEST_SRC_NAMESPACE_HPP

namespace stream9::ranges {}
namespace stream9::json {}

namespace testing {

namespace st9 = stream9;

namespace rng = st9::ranges;

namespace json = st9::json;

} // namespace testing

#endif // STREAM9_RANGES_TEST_SRC_NAMESPACE_HPP
