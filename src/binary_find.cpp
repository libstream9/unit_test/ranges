#include <stream9/ranges/binary_find.hpp>

#include "namespace.hpp"

#include <stream9/array.hpp>
#include <stream9/string.hpp>
#include <stream9/ranges/index_at.hpp>

#include <ranges>

#include <boost/test/unit_test.hpp>

namespace testing {

using stream9::array;
using stream9::string;

BOOST_AUTO_TEST_SUITE(binary_find_first_)

    BOOST_AUTO_TEST_CASE(found_left_)
    {
        std::vector<int> v { 1, 2, 3, 4, 5 };

        auto const it = rng::binary_find_first(v, 1);

        BOOST_REQUIRE(it != v.end());
        BOOST_TEST(rng::index_at(v, it) == 0);
    }

    BOOST_AUTO_TEST_CASE(found_middle_)
    {
        std::vector<int> v { 1, 2, 3, 4, 5 };

        auto const it = rng::binary_find_first(v, 4);

        BOOST_REQUIRE(it != v.end());
        BOOST_TEST(rng::index_at(v, it) == 3);
    }

    BOOST_AUTO_TEST_CASE(found_right_)
    {
        std::vector<int> v { 1, 2, 3, 4, 5 };

        auto const it = rng::binary_find_first(v, 5);

        BOOST_REQUIRE(it != v.end());
        BOOST_TEST(rng::index_at(v, it) == 4);
    }

    BOOST_AUTO_TEST_CASE(not_found_left_)
    {
        std::vector<int> v { 1, 2, 3, 4, 5 };

        auto const it = rng::binary_find_first(v, -5);

        BOOST_REQUIRE(it == v.end());
    }

    BOOST_AUTO_TEST_CASE(not_found_middle_)
    {
        std::vector<int> v { 1, 2, 4, 5 };

        auto const it = rng::binary_find_first(v, 3);

        BOOST_REQUIRE(it == v.end());
    }

    BOOST_AUTO_TEST_CASE(not_found_right_)
    {
        std::vector<int> v { 1, 2, 3, 4, 5 };

        auto const it = rng::binary_find_first(v, 10);

        BOOST_REQUIRE(it == v.end());
    }

    BOOST_AUTO_TEST_CASE(found_duplicated_left_)
    {
        std::vector<int> v { 1, 1, 2, 3, 4 };

        auto const it = rng::binary_find_first(v, 1);

        BOOST_REQUIRE(it != v.end());
        BOOST_TEST(rng::index_at(v, it) == 0);
    }

    BOOST_AUTO_TEST_CASE(found_duplicated_middle_)
    {
        std::vector<int> v { 1, 2, 3, 3, 4 };

        auto const it = rng::binary_find_first(v, 3);

        BOOST_REQUIRE(it != v.end());
        BOOST_TEST(rng::index_at(v, it) == 2);
    }

    BOOST_AUTO_TEST_CASE(found_duplicated_right_)
    {
        std::vector<int> v { 1, 2, 3, 4, 4 };

        auto const it = rng::binary_find_first(v, 4);

        BOOST_REQUIRE(it != v.end());
        BOOST_TEST(rng::index_at(v, it) == 3);
    }

    BOOST_AUTO_TEST_CASE(string_)
    {
        array<string> a1 { "bar", "foo" };

        auto it = rng::binary_find_first(a1, "foo");
        BOOST_REQUIRE(it != a1.end());
        BOOST_TEST(rng::index_at(a1, it) == 1);
    }

BOOST_AUTO_TEST_SUITE_END() // binary_find_first_

BOOST_AUTO_TEST_SUITE(binary_find_last_)

    BOOST_AUTO_TEST_CASE(found_left_)
    {
        std::vector<int> v { 1, 2, 3, 4, 5 };

        auto const it = rng::binary_find_last(v, 1);

        BOOST_REQUIRE(it != v.end());
        BOOST_TEST(rng::index_at(v, it) == 0);
    }

    BOOST_AUTO_TEST_CASE(found_middle_)
    {
        std::vector<int> v { 1, 2, 3, 4, 5 };

        auto const it = rng::binary_find_last(v, 3);

        BOOST_REQUIRE(it != v.end());
        BOOST_TEST(rng::index_at(v, it) == 2);
    }

    BOOST_AUTO_TEST_CASE(found_right_)
    {
        std::vector<int> v { 1, 2, 3, 4, 5 };

        auto const it = rng::binary_find_last(v, 5);

        BOOST_REQUIRE(it != v.end());
        BOOST_TEST(rng::index_at(v, it) == 4);
    }

    BOOST_AUTO_TEST_CASE(found_duplicated_left_)
    {
        std::vector<int> v { 1, 1, 2, 3, 4 };

        auto const it = rng::binary_find_last(v, 1);

        BOOST_REQUIRE(it != v.end());
        BOOST_TEST(rng::index_at(v, it) == 1);
    }

    BOOST_AUTO_TEST_CASE(found_duplicated_middle_)
    {
        std::vector<int> v { 1, 2, 3, 3, 4 };

        auto const it = rng::binary_find_last(v, 3);

        BOOST_REQUIRE(it != v.end());
        BOOST_TEST(rng::index_at(v, it) == 3);
    }

    BOOST_AUTO_TEST_CASE(found_duplicated_right_)
    {
        std::vector<int> v { 1, 2, 3, 4, 4 };

        auto const it = rng::binary_find_last(v, 4);

        BOOST_REQUIRE(it != v.end());
        BOOST_TEST(rng::index_at(v, it) == 4);
    }

    BOOST_AUTO_TEST_CASE(not_found_left_)
    {
        std::vector<int> v { 1, 2, 3, 4, 5 };

        auto const it = rng::binary_find_last(v, 0);

        BOOST_REQUIRE(it == v.end());
    }

    BOOST_AUTO_TEST_CASE(not_found_middle_)
    {
        std::vector<int> v { 1, 2, 4, 5 };

        auto const it = rng::binary_find_last(v, 3);

        BOOST_REQUIRE(it == v.end());
    }

    BOOST_AUTO_TEST_CASE(not_found_right_)
    {
        std::vector<int> v { 1, 2, 3, 4, 5 };

        auto const it = rng::binary_find_last(v, 6);

        BOOST_REQUIRE(it == v.end());
    }

BOOST_AUTO_TEST_SUITE_END() // binary_find_last_

} // namespace testing
