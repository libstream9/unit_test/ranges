#include <stream9/ranges/prefix.hpp>

#include "namespace.hpp"

#include <stream9/errors.hpp>
#include <stream9/json/value_from.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(prefix_)

    BOOST_AUTO_TEST_CASE(case_1_)
    {
        int arr[] = { 1, 2, 3, 4, 5 };

        auto x = rng::prefix(arr, 3);

        json::array expected { 1, 2, 3 };
        BOOST_TEST(json::value_from(x) == expected);
    }

    BOOST_AUTO_TEST_CASE(case_2_)
    {
        int arr[] = { 1, 2, 3, 4, 5 };

        auto x = rng::prefix(arr, 0);

        json::array expected { };
        BOOST_TEST(json::value_from(x) == expected);
    }

    BOOST_AUTO_TEST_CASE(case_3_)
    {
        int arr[] = { 1, 2, 3, 4, 5 };

        auto x = rng::prefix(arr, 5);

        json::array expected { 1, 2, 3, 4, 5 };
        BOOST_TEST(json::value_from(x) == expected);
    }

BOOST_AUTO_TEST_SUITE_END() // prefix_

} // namespace testing
