#include <stream9/ranges/is_prefix.hpp>

#include "namespace.hpp"

#include <string>
#include <string_view>
#include <vector>
#include <list>

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(is_prefix_)

    BOOST_AUTO_TEST_CASE(empty_1_)
    {
        std::string s1;
        std::string_view s2;

        BOOST_TEST(rng::is_prefix(s1, s2));
    }

    BOOST_AUTO_TEST_CASE(empty_2_)
    {
        std::string s1;
        std::string_view s2 = "foo";

        BOOST_TEST(rng::is_prefix(s1, s2));
    }

    BOOST_AUTO_TEST_CASE(empty_3_)
    {
        std::string s1 = "foo";
        std::string_view s2;

        BOOST_TEST(!rng::is_prefix(s1, s2));
    }

    BOOST_AUTO_TEST_CASE(string_1_)
    {
        std::string s1 = "foo";
        std::string_view s2 = "foobar";

        BOOST_TEST(rng::is_prefix(s1, s2));
    }

    BOOST_AUTO_TEST_CASE(string_2_)
    {
        std::string s1 = "foo";
        std::string_view s2 = "foo";

        BOOST_TEST(rng::is_prefix(s1, s2));
    }

    BOOST_AUTO_TEST_CASE(string_3_)
    {
        std::string s1 = "foobar";
        std::string_view s2 = "foo";

        BOOST_TEST(!rng::is_prefix(s1, s2));
    }

    BOOST_AUTO_TEST_CASE(vector_1_)
    {
        std::vector<int> v1 { 1, };
        std::list<int> v2 { 1, 2, };

        BOOST_TEST(rng::is_prefix(v1, v2));
    }

    BOOST_AUTO_TEST_CASE(vector_2_)
    {
        std::vector<int> v1 { 1, };
        std::list<int> v2 { 1, };

        BOOST_TEST(rng::is_prefix(v1, v2));
    }

    BOOST_AUTO_TEST_CASE(vector_3_)
    {
        std::vector<int> v1 { 2, 1, };
        std::list<int> v2 { 1, };

        BOOST_TEST(!rng::is_prefix(v1, v2));
    }

BOOST_AUTO_TEST_SUITE_END() // is_prefix_

BOOST_AUTO_TEST_SUITE(is_proper_prefix_)

    BOOST_AUTO_TEST_CASE(empty_1_)
    {
        std::string s1;
        std::string_view s2;

        BOOST_TEST(!rng::is_proper_prefix(s1, s2));
    }

    BOOST_AUTO_TEST_CASE(empty_2_)
    {
        std::string s1;
        std::string_view s2 = "foo";

        BOOST_TEST(rng::is_proper_prefix(s1, s2));
    }

    BOOST_AUTO_TEST_CASE(empty_3_)
    {
        std::string s1 = "foo";
        std::string_view s2;

        BOOST_TEST(!rng::is_proper_prefix(s1, s2));
    }

    BOOST_AUTO_TEST_CASE(string_1_)
    {
        std::string_view s1 = "foo";
        std::string s2 = "foobar";

        BOOST_TEST(rng::is_proper_prefix(s1, s2));
    }

    BOOST_AUTO_TEST_CASE(string_2_)
    {
        std::string_view s1 = "foo";
        std::string s2 = "foo";

        BOOST_TEST(!rng::is_proper_prefix(s1, s2));
    }

    BOOST_AUTO_TEST_CASE(string_3_)
    {
        std::string_view s1 = "foobar";
        std::string s2 = "foo";

        BOOST_TEST(!rng::is_proper_prefix(s1, s2));
    }

    BOOST_AUTO_TEST_CASE(vector_1_)
    {
        std::list<int> v1 { 1, };
        std::vector<int> v2 { 1, 2, };

        BOOST_TEST(rng::is_proper_prefix(v1, v2));
    }

    BOOST_AUTO_TEST_CASE(vector_2_)
    {
        std::list<int> v1 { 1, };
        std::vector<int> v2 { 1, };

        BOOST_TEST(!rng::is_proper_prefix(v1, v2));
    }

    BOOST_AUTO_TEST_CASE(vector_3_)
    {
        std::list<int> v1 { 2, 1, };
        std::vector<int> v2 { 1, };

        BOOST_TEST(!rng::is_proper_prefix(v1, v2));
    }

BOOST_AUTO_TEST_SUITE_END() // is_proper_prefix_

} // namespace testing
