#ifndef STREAM9_RANGES_TEST_SRC_OSTREAM_TUPLE_HPP
#define STREAM9_RANGES_TEST_SRC_OSTREAM_TUPLE_HPP

#include <ostream>
#include <tuple>

namespace std { //XXX yeah, it's illegal, I know

template<typename... Ts>
inline std::ostream&
operator<<(std::ostream& os, std::tuple<Ts...> const& v)
{
    auto fn = [] <size_t... I>
        (auto& os, auto&& v, std::index_sequence<I...>) {
            ((os << (I == 0 ? "" : ", ") << std::get<I>(v)), ...);
        };

    os << '(';
    fn(os, v, std::index_sequence_for<Ts...>());
    os << ')';

    return os;
}

} // namespace std

#endif // STREAM9_RANGES_TEST_SRC_OSTREAM_TUPLE_HPP
