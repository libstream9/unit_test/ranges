#include <stream9/range.hpp>

#include "namespace.hpp"

#include <stream9/array.hpp>
#include <stream9/json.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

using stream9::array;
using stream9::range;

BOOST_AUTO_TEST_SUITE(range_)

    BOOST_AUTO_TEST_CASE(construct_from_c_array_)
    {
        int a[] = { 0, 1, 2, 3 };

        range<int*> r { a };

        BOOST_TEST(r.size() == sizeof(a) / sizeof(int));
    }

    BOOST_AUTO_TEST_CASE(construct_from_range_)
    {
        array<int> a { 0, 1, 2, 3 };

        range<int*> r { a };

        BOOST_TEST(r.size() == a.size());
    }

    BOOST_AUTO_TEST_CASE(construct_from_iterator_)
    {
        array<int> a { 0, 1, 2, 3 };

        range<int*> r { a.begin(), a.end() };

        BOOST_TEST(r.size() == a.size());
    }

    BOOST_AUTO_TEST_CASE(deduction_guide_)
    {
        array<int> a { 0, 1, 2, 3 };

        range r { a };

        static_assert(std::same_as<decltype(r), range<int*>>);
    }

    BOOST_AUTO_TEST_CASE(structured_binding_)
    {
        array<int> a { 0, 1, 2, 3 };

        range r { a };

        auto [f, l] = r;

        BOOST_TEST(f == r.begin());
        BOOST_TEST(l == r.end());
    }

BOOST_AUTO_TEST_SUITE_END() // range_

} // namespace testing
