#include <stream9/ranges/accumulate.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

using stream9::accumulate;

BOOST_AUTO_TEST_SUITE(accumulate_)

    BOOST_AUTO_TEST_SUITE(type_0_)

        BOOST_AUTO_TEST_CASE(step_1_)
        {
            std::vector<int> v { 1, 2, 3, 4 };

            auto acc = accumulate(v);

            BOOST_TEST(acc == 10);
        }

    BOOST_AUTO_TEST_SUITE_END() // type_0_

    BOOST_AUTO_TEST_SUITE(type_1_)

        BOOST_AUTO_TEST_CASE(step_1_)
        {
            std::vector<int> v { 1, 2, 3, 4 };

            auto acc = accumulate(v, 10);

            BOOST_TEST(acc == 20);
        }

        BOOST_AUTO_TEST_CASE(step_2_)
        {
            std::vector<int> v { 1, 2, 3, 4 };

            auto twice = [](auto x) { return x * 2; };
            using std::ranges::views::transform;

            auto acc = accumulate(v | transform(twice), 10);

            BOOST_TEST(acc == 30);
        }

    BOOST_AUTO_TEST_SUITE_END() // type_1_

    BOOST_AUTO_TEST_SUITE(type_2_)

        BOOST_AUTO_TEST_CASE(step_1_)
        {
            std::vector<int> v { 1, 2, 3, 4 };

            auto twice = [](auto x) { return x * 2; };
            auto acc = accumulate(v, 10, twice);

            BOOST_TEST(acc == 30);
        }

    BOOST_AUTO_TEST_SUITE_END() // type_2_

    BOOST_AUTO_TEST_SUITE(type_3_)

        BOOST_AUTO_TEST_CASE(step_1_)
        {
            std::vector<int> v { 1, 2, 3, 4 };

            auto acc = accumulate(v, 10, std::identity(), std::plus<int>());

            BOOST_TEST(acc == 20);
        }

    BOOST_AUTO_TEST_SUITE_END() // type_3_

BOOST_AUTO_TEST_SUITE_END() // accumulate_

} // namespace testing
