#include <stream9/ranges/views/indexed.hpp>

#include "../namespace.hpp"
#include "../ostream_tuple.hpp"

#include <algorithm>
#include <string>
#include <vector>

#include <boost/test/unit_test.hpp>

namespace testing {

inline constexpr boost::test_tools::per_element per_element;

//TODO resume testing when gcc 10.3 comes out
BOOST_AUTO_TEST_SUITE(indexed_)

BOOST_AUTO_TEST_CASE(basic_)
{
    std::string s1 = "abcdefg";

    auto v = rng::views::indexed(s1);

    std::vector<std::tuple<int, char>> result;
    std::ranges::copy(v, std::back_inserter(result));

    auto const expected = {
        std::tuple<int, char> { 0, 'a' }, { 1, 'b' }, { 2, 'c' },
        { 3, 'd' }, { 4, 'e' }, { 5, 'f' }, { 6, 'g' },
    };

    BOOST_TEST(result == expected, per_element);
}

BOOST_AUTO_TEST_SUITE_END() // indexed_

} // namespace testing
