#include <stream9/ranges/views/zip.hpp>

#include "../namespace.hpp"
#include "../ostream_tuple.hpp"

#include <algorithm>
#include <string>
#include <vector>

#include <boost/test/unit_test.hpp>

namespace testing {

inline constexpr boost::test_tools::per_element per_element;

BOOST_AUTO_TEST_SUITE(zip_)

template<class> struct type_of;

BOOST_AUTO_TEST_CASE(basic_)
{
    std::vector<int> t1 { 1, 2, 3, };
    std::string t2 = "abcd";

    auto v = rng::views::zip(t1, t2);

    auto const expected = {
        std::make_tuple(1, 'a'), { 2, 'b' }, { 3, 'c' },
    };

    BOOST_TEST(v == expected, per_element);
}

BOOST_AUTO_TEST_SUITE_END() // zip_

} // namespace testing
