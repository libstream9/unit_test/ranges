#include <stream9/ranges/views/map.hpp>

#include "../namespace.hpp"

#include <string>

#include <boost/test/unit_test.hpp>

#include <stream9/array.hpp>
#include <stream9/json.hpp>

namespace testing {


using std::string;
using stream9::array;
using stream9::ranges::views::map;

BOOST_AUTO_TEST_SUITE(map_view_)

    BOOST_AUTO_TEST_CASE(step_1_)
    {
        using T = array<string>;
        T a { "foo", "bar", "xyzzy" };

        map m { a, &string::size };

        json::array expected { 3, 3, 5 };
        auto r = json::value_from(m);

        BOOST_TEST(r == expected);
    }

BOOST_AUTO_TEST_SUITE_END() // map_view_

} // namespace testing
