#include <stream9/find.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

#include <stream9/array.hpp>
#include <stream9/less.hpp>
#include <stream9/projection.hpp>
#include <stream9/unique_array.hpp>

namespace testing {

using stream9::array;
using stream9::unique_array;

BOOST_AUTO_TEST_SUITE(find_)

    BOOST_AUTO_TEST_CASE(find_1_)
    {
        array<int> a { 100, 200 };

        auto i1 = rng::find(a, 100);
        BOOST_CHECK(i1 == a.begin());

        auto i2 = rng::find(a, 300);
        BOOST_CHECK(i2 == a.end());
    }

    BOOST_AUTO_TEST_CASE(find_2_)
    {
        array<int> a { 100, 200 };

        auto i1 = rng::find(a.begin(), a.end(), 100);
        BOOST_CHECK(i1 == a.begin());

        auto i2 = rng::find(a.begin(), a.end(), 300);
        BOOST_CHECK(i2 == a.end());
    }

    BOOST_AUTO_TEST_CASE(find_3_)
    {
        using stream9::less;
        using stream9::project;

        struct foo { int x; int y; };
        unique_array<foo, less, project<&foo::y>> a;

        a.insert({100, 200});
        a.insert({200, 100});

        auto i1 = rng::find(a, 100);
        BOOST_CHECK(i1 == a.begin());

        auto i2 = rng::find(a, 300);
        BOOST_CHECK(i2 == a.end());
    }

    BOOST_AUTO_TEST_CASE(find_4_)
    {
        using stream9::less;
        using stream9::project;

        struct foo { int x; int y; };
        unique_array<foo, less, project<&foo::y>> a;

        a.insert({100, 200});
        a.insert({200, 100});

        auto i1 = rng::find(a, 100, project<&foo::x>());
        BOOST_CHECK(i1 == a.begin() + 1);

        auto i2 = rng::find(a, 200, project<&foo::x>());
        BOOST_CHECK(i2 == a.begin());

        auto i3 = rng::find(a, 300, project<&foo::x>());
        BOOST_CHECK(i3 == a.end());
    }

BOOST_AUTO_TEST_SUITE_END() // find_

} // namespace testing
