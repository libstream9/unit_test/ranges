#include <stream9/ranges/range_facade.hpp>

#include <stream9/ranges/empty.hpp>

#include <concepts>
#include <ranges>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace rng = stream9::ranges;

BOOST_AUTO_TEST_SUITE(range_facade_)

    class my_range : public rng::range_facade
    {
    public:
        my_range() { m_v = { 5, 4, 3, 2, 1 }; }

        auto begin() { return m_v.begin(); }
        auto begin() const { return m_v.begin(); }
        auto end() { return m_v.end(); }
        auto end() const { return m_v.end(); }

    private:
        std::vector<int> m_v;
    };

    BOOST_AUTO_TEST_CASE(concepts_)
    {
        static_assert(rng::contiguous_range<my_range>);
    }

    BOOST_AUTO_TEST_CASE(size_)
    {
        my_range v;

        static_assert(std::same_as<decltype(v.size()),
                                   rng::size_t<my_range> >);

        BOOST_TEST(v.size() == 5);
        BOOST_TEST(stream9::ranges::size(v) == 5);
        BOOST_TEST(std::ranges::size(v) == 5);

        using std::ranges::size;
        BOOST_TEST(size(v) == 5);
    }

    BOOST_AUTO_TEST_CASE(empty_)
    {
        my_range v;

        BOOST_TEST(!v.empty());
        BOOST_TEST(!stream9::ranges::empty(v));
        BOOST_TEST(!std::ranges::empty(v));

        using std::ranges::empty;
        BOOST_TEST(!empty(v));
    }

    BOOST_AUTO_TEST_CASE(at_)
    {
        my_range v;

        static_assert(std::same_as<decltype(v.at(0)), int&>);

        BOOST_TEST(v.at(0) == 5);
        BOOST_TEST(v.at(1) == 4);
        BOOST_TEST(v.at(2) == 3);
        BOOST_TEST(v.at(3) == 2);
        BOOST_TEST(v.at(4) == 1);

        BOOST_CHECK_EXCEPTION(
            v.at(-1),
            stream9::error,
            [&](auto&& e) {
                BOOST_TEST(e.why() == rng::errc::out_of_range);
                return true;
            });

        BOOST_CHECK_EXCEPTION(
            v.at(5),
            stream9::error,
            [&](auto&& e) {
                BOOST_TEST(e.why() == rng::errc::out_of_range);
                return true;
            });
    }

    BOOST_AUTO_TEST_CASE(bracket_)
    {
        my_range v;

        static_assert(std::same_as<decltype(v[0]), int&>);

        BOOST_TEST(v[0] == 5);
        BOOST_TEST(v[1] == 4);
        BOOST_TEST(v[2] == 3);
        BOOST_TEST(v[3] == 2);
        BOOST_TEST(v[4] == 1);
    }

    BOOST_AUTO_TEST_CASE(bracket_const_)
    {
        my_range const v;

        static_assert(std::same_as<decltype(v[0]), int const&>);

        BOOST_TEST(v[0] == 5);
        BOOST_TEST(v[1] == 4);
        BOOST_TEST(v[2] == 3);
        BOOST_TEST(v[3] == 2);
        BOOST_TEST(v[4] == 1);
    }

    BOOST_AUTO_TEST_CASE(data_)
    {
        my_range v;

        static_assert(std::same_as<decltype(v.data()), int*>);

        auto ptr = v.data();
        BOOST_TEST(ptr != nullptr);
    }

    BOOST_AUTO_TEST_CASE(data_const_)
    {
        my_range const v;

        static_assert(std::same_as<decltype(v.data()), int const*>);

        auto ptr = v.data();
        BOOST_TEST(ptr != nullptr);
    }

    BOOST_AUTO_TEST_CASE(front_)
    {
        my_range v;

        static_assert(std::same_as<decltype(v.front()), int&>);

        auto& i = v.front();
        BOOST_TEST(i == 5);
    }

    BOOST_AUTO_TEST_CASE(front_const_)
    {
        my_range const v;

        static_assert(std::same_as<decltype(v.front()), int const&>);

        auto& i = v.front();
        BOOST_TEST(i == 5);
    }

    BOOST_AUTO_TEST_CASE(back_)
    {
        my_range v;

        static_assert(std::same_as<decltype(v.back()), int&>);

        auto& i = v.back();
        BOOST_TEST(i == 1);
    }

    BOOST_AUTO_TEST_CASE(back_const_)
    {
        my_range const v;

        static_assert(std::same_as<decltype(v.back()), int const&>);

        auto& i = v.back();
        BOOST_TEST(i == 1);
    }

    BOOST_AUTO_TEST_CASE(structured_binding_)
    {
        my_range v;

        auto& [it, end] = v;

        BOOST_CHECK(v.begin() == it);
        BOOST_CHECK(v.end() == end);
    }

BOOST_AUTO_TEST_SUITE_END() // range_facade_

} // namespace testing
